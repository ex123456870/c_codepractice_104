#include "api_demo.h"
#define DEFINE_TEST (1<<2)
#define DEFINE_TEST2(i) (i*3)
#define DEFINE_TEST3	ArrayForDefineTest[2]
//const UINT32 ArrayForDefineTest[3]={1,2,3};
UINT32 ArrayForDefineTest[3]={1,2,3};
static sHASH_TABLE *sHashTable=NULL;
UINT32 lValue=1;

UINT32 main()
{
#if 0
	UINT8 *pcSpain,*pcItaly;
	if(!(sHashTable=CreateHashTable(16,NULL)))
	{
		printf("ERROR: CreateHashTable() failed\n");
		exit(EXIT_FAILURE);
	}
	InsertElement(sHashTable,"Italy","Rome");
	InsertElement(sHashTable,"Spain","Madrid");
	printf("After insert:\n");
	pcItaly=GetValue(sHashTable,"Italy");
	printf("key:Italy,value: %s\n",pcItaly?pcItaly:"-");
	pcSpain=GetValue(sHashTable,"Spain");
	printf("key:Spain,value: %s\n",pcSpain?pcSpain:"-");
	RemoveElement(sHashTable,"Spain");
	printf("After remove:\n");
	pcSpain=GetValue(sHashTable,"Spain");
	printf("key:Spain,value: %s\n",pcSpain?pcSpain:"-");
	ResizeHashTable(sHashTable,8);
	printf("After resize:\n");
	pcItaly=GetValue(sHashTable,"Italy");
	printf("key:Italy,value: %s\n",pcItaly?pcItaly:"-");
	DestroyHashTable(sHashTable);
#else
	UINT32 lValue=2,lNum=0x555;//0xAAA;	//當local跟global變數同名稱時，會優先使用local變數
	UINT8 cIndex,cShiftValue=6,*ptr=malloc(sizeof(UINT32));
	
	ptr=&lValue;
	LogPrintf("*ptr=%u,lNum&(1<<cShiftValue)=0x%x\n",*ptr,lNum&(1<<cShiftValue));
	/*lNum=100;
	LogPrintf("DEFINE_TEST=0x%x\n",DEFINE_TEST);
	LogPrintf("~DEFINE_TEST=0x%x\n",(UINT16)~DEFINE_TEST);
	LogPrintf("!DEFINE_TEST=0x%x\n",!DEFINE_TEST);
	LogPrintf("DEFINE_TEST2(1)=0x%x\n",DEFINE_TEST2(1));
	LogPrintf("DEFINE_TEST3=0x%x\n",DEFINE_TEST3);
	DEFINE_TEST3=lNum;
	ROY_VARD(DEFINE_TEST3);//const就不能修改
	ROY_VARD(ArrayForDefineTest[2]);
	LogPrintf("DEFINE_TEST3=0x%x\n",DEFINE_TEST3);//define可以被修改
	LogPrintf("ArrayForDefineTest[2]=0x%x\n",ArrayForDefineTest[2]);
	*/
#endif
	//while(1)
	{
		API_test();
	}
	//printf("Hello world!\n");
	return 0;
}
