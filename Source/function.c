#include "api_demo.h"

void (*FuncPointerVoid)(void);
void (*FuncPointerVoidTable[2][2])(void);

static UINT32 DefHashFunc(const UINT8 *pcKey)
{
	UINT32 lHash=0;
	while(*pcKey)
		lHash+=(UINT8)*pcKey++;
	return lHash;
}

static UINT8 *MyStrDup(const UINT8 *pcKey)
{
	UINT8 *pcMemory;
	if(!(pcMemory=(UINT8*)malloc(strlen(pcKey)+1)))
		return NULL;
	strcpy(pcMemory,pcKey);
	return pcMemory;
}

sHASH_TABLE *CreateHashTable(UINT32 lSize, UINT32 (*HashFunction)(const UINT8*))
{
	sHASH_TABLE *psHashtable;
	if(!(psHashtable=(sHASH_TABLE*)malloc(sizeof(sHASH_TABLE))))
		return NULL;
	if(!(psHashtable->psNodes=(sHashNode**)calloc(lSize,sizeof(sHashNode*))))
	{
		free(psHashtable);
		return NULL;
	}
	psHashtable->lSize=lSize;
	if(HashFunction)
		psHashtable->HashFunction=HashFunction;
	else
		psHashtable->HashFunction=DefHashFunc;
	return psHashtable;
}

void DestroyHashTable(sHASH_TABLE *psHashtable)
{
	UINT32 lIndex;
	sHashNode *psNode;
	sHashNode *psOldNode;
	for(lIndex=0;lIndex<psHashtable->lSize;++lIndex)
	{
		psNode=psHashtable->psNodes[lIndex];
		while(psNode)
		{
			free(psNode->pcKey);
			psOldNode=psNode;
			psNode=psNode->psNext;
			free(psOldNode);
		}
	}
	free(psHashtable->psNodes);
	free(psHashtable);
}

UINT32 InsertElement(sHASH_TABLE *psHashtable,const UINT8 *pcKey,void *Data)
{
	sHashNode *psNode;
	UINT32 lHash=psHashtable->HashFunction(pcKey)%psHashtable->lSize;
	psNode=psHashtable->psNodes[lHash];
	while(psNode)
	{
		if(!strcmp(psNode->pcKey,pcKey))
		{
			psNode->pData=Data;
			return 0;
		}
		psNode=psNode->psNext;
	}
	if(!(psNode=malloc(sizeof(sHashNode))))
		return -1;
	if(!(psNode->pcKey=MyStrDup(pcKey)))
	{
		free(psNode);
		return -1;
	}
	psNode->pData=Data;
	psNode->psNext=psHashtable->psNodes[lHash];
	psHashtable->psNodes[lHash]=psNode;
	return 0;
}
UINT32 RemoveElement(sHASH_TABLE *psHashtable,const UINT8 *pcKey)
{
	sHashNode *psNode=NULL;
	sHashNode *prevnode=NULL;
	UINT32 lHash=psHashtable->HashFunction(pcKey)%psHashtable->lSize;
	psNode=psHashtable->psNodes[lHash];
	while(psNode)
	{
		if(!strcmp(psNode->pcKey,pcKey))
		{
			free(psNode->pcKey);
			if(prevnode)
				prevnode->psNext=psNode->psNext;
			else
				psHashtable->psNodes[lHash]=psNode->psNext;
			free(psNode);
			return 0;
		}
		prevnode=psNode;
		psNode=psNode->psNext;
	}
	return -1;
}
void *GetValue(sHASH_TABLE *psHashtable,const UINT8 *pcKey)
{
	sHashNode *psNode;
	UINT32 lHash=psHashtable->HashFunction(pcKey)%psHashtable->lSize;
	psNode=psHashtable->psNodes[lHash];
	while(psNode)
	{
		if(!strcmp(psNode->pcKey,pcKey))
			return psNode->pData;
		psNode=psNode->psNext;
	}
	return NULL;
}
UINT32 ResizeHashTable(sHASH_TABLE *psHashtable,UINT32 lSize)
{
	sHASH_TABLE cNewtbl;
	UINT32 lIndex;
	sHashNode *psNode,*next;
	cNewtbl.lSize=lSize;
	cNewtbl.HashFunction=psHashtable->HashFunction;
	if(!(cNewtbl.psNodes=calloc(lSize,sizeof(sHashNode*))))
		return -1;
	for(lIndex=0;lIndex<psHashtable->lSize;++lIndex)
	{
		for(psNode=psHashtable->psNodes[lIndex];psNode;psNode=next)
		{
			next=psNode->psNext;
			InsertElement(&cNewtbl,psNode->pcKey,psNode->pData);
			RemoveElement(psHashtable,psNode->pcKey);
		}
	}
	free(psHashtable->psNodes);
	psHashtable->lSize=cNewtbl.lSize;
	psHashtable->psNodes=cNewtbl.psNodes;
	return 0;
}

void DrawInvertedTrangle(UINT8 cLevel)
{
	UINT8 cIndex,cNextLevel=cLevel-1;
	if (1==cLevel)
		printf("*\n");
	else
	{
		for(cIndex=cLevel;cLevel>0;cLevel--)
			printf("*");
		printf("\n");
		DrawInvertedTrangle(cNextLevel);
	}
}

void GetMemoryStringTest(void)
{
	UINT8 *cTemp;
	UINT8 *pcTr1,acString1[16]="now is the time";
	UINT8 cArray[16];
	cTemp=(UINT8 *)malloc(100*sizeof(UINT8));
	pcTr1="now is the time";
	#if 1
	strcpy(cArray,acString1);
	printf("Array = %s\n",cArray);
	#else
	strcpy(temp,String1);
	printf("temp = %s\n",temp);
	#endif

	printf("%s\n",pcTr1);
	free(cTemp);
}

/*---------------------------*/
//UINT8 *str=NULL;
//GetMemory(&str);
/*---------------------------*/
void GetMemory(UINT8 **pcGetMemory,UINT8 cLength)////透過雙重指標改變指標變數的值
{
	*pcGetMemory=(UINT8 *)malloc(cLength);//改變指標變數的值，即改變str存放的值
	LogPrintf("pcGetMemory=0x%x,*pcGetMemory=0x%x,**pcGetMemory=0x%x\n",pcGetMemory,*pcGetMemory,**pcGetMemory);
}

void IEEE754_Valf00(float fValue)
{
	UINT8 cIndex=0,cArray[4],cArraySize=LENGTH(cArray);
	ROY_VAR((UINT32)cArray);
	memcpy(cArray,&fValue,sizeof(float));
	LogPrintf("&fValue=0x%x,cArray=0x%x\n",(UINT32)&fValue,(UINT32)cArray);
	for (;cIndex<cArraySize;cIndex++)
		LogPrintf("cArray[%u]=0x%x\n",cIndex,cArray[cIndex]);
	printf("\n");
}

void IEEE754_Valf01(float *fValue)
{
	UINT32 *plValue;
	plValue=(UINT32 *)fValue;
	UINT32 lSign=0,lExp=0,lFraction=0;
	lSign=*plValue>>31;
	lExp=*plValue<<1>>1>>23;
	lFraction=*plValue<<1<<22>>23;
	LogPrintf("*plValue=0x%x,lSign=0x%x,lExp=0x%x,lFraction=0x%x\n\n",*plValue,lSign,lExp,lFraction);
}

void IEEE754_Valf02(float fValue)
{
	union{
		float fValue;
		UINT32 lValue;
	}uIEEE754;
	union{
		float fValue;
		struct{
			UINT32 lFraction:23;
			UINT32 lExp:8;
			UINT32 lSign:1;
		};
	}uIEEE754_ex;

	uIEEE754.fValue=fValue;
	uIEEE754_ex.fValue=fValue;
	LogPrintf("[uIEEE754]lValue=0x%x\n",uIEEE754.lValue);
	LogPrintf("[uIEEE754_ex]lSign=0x%x,lExp=0x%x,lFraction=0x%x\n\n",uIEEE754_ex.lSign,uIEEE754_ex.lExp,uIEEE754_ex.lFraction);
}

void IEEE754_Valf03(float fValue)
{
	UINT32 lValue;
	lValue=*(UINT32*)&fValue;
	LogPrintf("0x%x\n\n",lValue);
}

typedef struct
{
	UINT8  cVal;
	F64 f64val;
}ST_1;
void IEEE754_Valf04(F64 fValue)
{
	UINT8  cVal=0xAA,*ptr=NULL,cIndex;
	F64 f64val=fValue;

	ST_1 st1={0xBB,fValue};
	LogPrintf("&cVal=0x%p,&f64val=0x%p\n",&cVal,&f64val);
	ptr=&cVal;
	for(cIndex=0;cIndex<16;cIndex++)
		LogPrintf("0x%02x (0x%p)\n",*(ptr-cIndex),ptr-cIndex);		//Big-Endian
	LogPrintf("&st1.cVal=0x%p,&st1.f64val=0x%p\n",&st1.cVal,&st1.f64val);
	ptr=&st1.cVal;
	for(cIndex=0;cIndex<16;cIndex++)
		LogPrintf("0x%02x (0x%p)\n",*(ptr+cIndex),ptr+cIndex);		//Little-Endian
}


void PrintMatrix(UINT32 *plArray,UINT32 lCol,UINT32 lRow)
{
	ROY_VARD(COLUMN_PRINT_MATRIX*lRow+lCol);
}

void MatrixMul(UINT32 *plArray1,UINT32 *plArray2,UINT32 *plArray3)
{
	UINT32 i,j;
	for(i=0;i<ROW_MATRIX_MUL;i++)
	{
		for(j=0;j<COLUMN_MATRIX_MUL;j++)
		{
			plArray3[i*COLUMN_MATRIX_MUL+j]=plArray1[COLUMN_MATRIX_MUL*i]*plArray2[j]+plArray1[COLUMN_MATRIX_MUL*i+1]*plArray2[j+2];
			//ROY_VARD(i*COLUMN_MATRIX_MUL+j);
		}
	}
}

void MatrixAdd(UINT32 *plArray1,UINT32 *plArray2,UINT32 *plArray3)
{
	UINT32 i,CalLength=COLUMN_MATRIX_MUL*ROW_MATRIX_MUL;
	for(i=0;i<CalLength;i++)
	{
			plArray3[i]=plArray1[i]+plArray2[i];
			//ROY_VARD(i*COLUMN_MATRIX_MUL+j);
	}
}

void FuncPointTest_Odd(void)
{
	printf("Odd\n");
}

void FuncPointTest_Even(void)
{
	printf("Even\n");
}

void LinkListArray(void)
{
	struct Node_p
	{
		UINT8 cData[6];
		struct Node_p *pNext;
	};
	struct Node_p sNode1,sNode2;
	UINT8 cArray[50];
	LogPrintf("struct Node_p size=%d\n",sizeof(struct Node_p));
	LogPrintf("&a=0x%x, &b=0x%x\n",(UINT32)&sNode1,(UINT32)&sNode2);
	memcpy(sNode1.cData,"Hello",sizeof("Hello"));
	sNode1.pNext=NULL;
	memcpy(sNode2.cData,"World",sizeof("World"));
	sNode2.pNext=NULL;
	sNode1.pNext=&sNode2;
	ROY_STRING(sNode1.cData);
	ROY_STRING(sNode1.pNext->cData);
	memcpy(cArray,strcat(sNode1.cData, sNode2.cData),12);
	ROY_STRING(cArray);
}

sLINK_LIST_NODE *GetNode(void)
{
	sLINK_LIST_NODE *psNewNode=NULL;

	psNewNode=(sLINK_LIST_NODE *)malloc(sizeof(sLINK_LIST_NODE));
	if(NULL==psNewNode)
		LogPrintf("Not enough memory!!\n");
	return psNewNode;
}

void LinkListTraverse(sLINK_LIST_NODE *psLeadNode)
{
	sLINK_LIST_NODE *psTemp=psLeadNode;
	while(NULL!=psTemp)
	{
		printf("%d \t",psTemp->lData);
		psTemp=psTemp->psNext;
	}
	printf("\n");
}

UINT32 InsertNodeAfterNode(sLINK_LIST_NODE *psLeadNode,sLINK_LIST_NODE *psOrderNode,UINT32 lData)
{
	sLINK_LIST_NODE *psNewNode=GetNode();
	if(NULL==psNewNode)
		return FALSE;
	psNewNode->lData=lData;
	psNewNode->psNext=NULL;
	if(NULL!=psOrderNode)
	{
		psNewNode->psNext=psOrderNode->psNext;
		psOrderNode->psNext=psNewNode;
	}
	else
		psLeadNode=psNewNode;
	LogPrintf(".....OK!\n");
	return TRUE;
}

UINT32 InsertNodeBeforeNode(sLINK_LIST_NODE *psLeadNode,sLINK_LIST_NODE *psOrderNode,UINT32 lData)
{
	sLINK_LIST_NODE *psNewNode=GetNode();
	if(NULL==psNewNode)
		return FALSE;
	psNewNode->lData=lData;
	psNewNode->psNext=NULL;
	while((NULL!=psLeadNode->psNext) && (psLeadNode->psNext!=psOrderNode))
	{
		psLeadNode=psLeadNode->psNext;
	}
	psNewNode->psNext=psLeadNode->psNext;
	psLeadNode->psNext=psNewNode;
	LogPrintf("....OK!\n");
	return TRUE;
}

sLINK_LIST_NODE * PreNode(sLINK_LIST_NODE *psLeadNode,sLINK_LIST_NODE *psOrderNode)
{
	sLINK_LIST_NODE *psBackupPtr=psLeadNode;
	while((NULL!=psBackupPtr) && (psOrderNode!=psBackupPtr->psNext))
		 psBackupPtr=psBackupPtr->psNext;
	return psBackupPtr;
}

UINT32 DeleteNodeAfterNode(sLINK_LIST_NODE *psLeadNode,sLINK_LIST_NODE *psOrderNode)
{
	sLINK_LIST_NODE *psBackupPtr=psLeadNode,*psTempNote=NULL;
	if (psOrderNode==psLeadNode)
		 exit(1);		// exit program on error
#if 1
	while((NULL!=psBackupPtr) && (psOrderNode->psNext!=psBackupPtr->psNext))
		psBackupPtr=psBackupPtr->psNext;
#else
	psBackupPtr=PreNode(psLeadNode,psOrderNode);
#endif
	if (NULL==psBackupPtr)
		return FALSE;
	//LogPrintf("psOrderNode=0x%x,\t\tpsOrderNode->lData=%d\n",psOrderNode,psOrderNode->lData);
	//LogPrintf("psOrderNode->psNext=0x%x,\tpsOrderNode->psNext->lData=%d\n",psOrderNode->psNext,psOrderNode->psNext->lData);
	//LogPrintf("psOrderNode->psNext->psNext=0x%x,psOrderNode->psNext->psNext->lData=%d\n",psOrderNode->psNext->psNext,psOrderNode->psNext->psNext->lData);
	psTempNote=psOrderNode->psNext;		// 999
	psOrderNode->psNext=psOrderNode->psNext->psNext;	// 999 next
	free(psTempNote);	//多個temp而999的next要為null，不然會free到後面的
	LogPrintf(".....OK!\n");
	return TRUE;
}

UINT32 DeleteNodeBeforeNode(sLINK_LIST_NODE *psLeadNode,sLINK_LIST_NODE *psOrderNode)
{
	sLINK_LIST_NODE *psBackupPtr;
	if (psOrderNode==psLeadNode)
		 exit(1);		//return ERROR
	while((NULL!=psLeadNode->psNext) && (psLeadNode->psNext!=psOrderNode))
	{
		psBackupPtr=psLeadNode;
		psLeadNode=psLeadNode->psNext;
	}
	free(psLeadNode);
	psLeadNode=psBackupPtr;
	psLeadNode->psNext=psOrderNode;
	LogPrintf(".....OK!\n");
	return TRUE;
}

sLINK_LIST_NODE *ConcatenateLinkedList(sLINK_LIST_NODE *psLeadNode1,sLINK_LIST_NODE *psLeadNode2)
{
	sLINK_LIST_NODE *psBackupPtr=psLeadNode1;
	if(psBackupPtr != NULL)
	{
		while(NULL!=psBackupPtr->psNext)
			psBackupPtr=psBackupPtr->psNext;
		psBackupPtr->psNext=psLeadNode2;
	}
	else
		psLeadNode1=psLeadNode2;
	LogPrintf(".....OK!\n");
	return psLeadNode1;
}

sLINK_LIST_NODE *CreateLinkedList(UINT32 *plData,SINT32 slSize)
{
	sLINK_LIST_NODE *psLeadNode=NULL;
	SINT32 slIndex;
	for(slIndex=slSize-1;slIndex>=0;slIndex--)
	{
		sLINK_LIST_NODE *psNode=GetNode();
		if(NULL==psNode)
			exit(1);		//return ERROR
		psNode->lData=plData[slIndex];
		psNode->psNext=psLeadNode;
		psLeadNode=psNode;
		LogPrintf("Insert a node on list lead(Data: %d).....OK!\n",plData[slIndex]);
	}
	return psLeadNode;
}

void PushFrontLinkList(sLINK_LIST_NODE_p **pFirstPtr)
{
	sLINK_LIST_NODE_p *psNewNode=NULL;
	psNewNode=(sLINK_LIST_NODE_p *)malloc(sizeof(sLINK_LIST_NODE_p));
	LogPrintf("NewNode=0x%x,&(NewNode->cData)=0x%x\n",psNewNode,&(psNewNode->cData));
	LogPrintf("pFirstPtr=0x%x,*pFirstPtr=0x%x,**pFirstPtr=0x%x\n",pFirstPtr,*pFirstPtr,**pFirstPtr);
	psNewNode->psNext=*pFirstPtr;
	*pFirstPtr=psNewNode;
	LogPrintf("pFirstPtr=0x%x,*pFirstPtr=0x%x,**pFirstPtr=0x%x\n",pFirstPtr,*pFirstPtr,**pFirstPtr);
}

void PushRearLinkList(sLINK_LIST_NODE_p **psFirstPtr)
{
	sLINK_LIST_NODE_p *psNewNode=NULL,*psBackupPtr=NULL;
	psNewNode=(sLINK_LIST_NODE_p *)malloc(sizeof(sLINK_LIST_NODE_p));
	psNewNode->psNext=NULL;
	LogPrintf("NewNode=0x%x,&(NewNode->cData)=0x%x\n",psNewNode,&(psNewNode->cData));
	LogPrintf("pFirstPtr=0x%x,*pFirstPtr=0x%x,**pFirstPtr=0x%x\n",psFirstPtr,*psFirstPtr,**psFirstPtr);
	LogPrintf("&(**pFirstPtr)=0x%x,&(*pFirstPtr)->cData=0x%x\n",&(**psFirstPtr),&(*psFirstPtr)->cData);
	LogPrintf("**pFirstPtr=%s\n",**psFirstPtr);
	LogPrintf("(*pFirstPtr)->cData=%s\n",(*psFirstPtr)->cData);
	while(NULL!=*psFirstPtr)	// Traversal
	{
		LogPrintf("*pFirstPtr=0x%x,%s\n",*psFirstPtr,**psFirstPtr);
		psBackupPtr=*psFirstPtr;
		*psFirstPtr=(*psFirstPtr)->psNext;
	}
	*psFirstPtr=psBackupPtr;
	(*psFirstPtr)->psNext=psNewNode;
	*psFirstPtr=(*psFirstPtr)->psNext;
	LogPrintf("pFirstPtr=0x%x,*pFirstPtr=0x%x,**pFirstPtr=0x%x\n",psFirstPtr,*psFirstPtr,**psFirstPtr);
}

void LinkListPointer(void)
{
	sLINK_LIST_NODE_p *psNode1=NULL,*psNode2=NULL,*psFirstPtr=NULL,*psFirstPtrBackUp=NULL;
	UINT8 cIndex=0;
	ROY_VARD(sizeof(sLINK_LIST_NODE_p));
	psNode1=(sLINK_LIST_NODE_p *)malloc(sizeof(sLINK_LIST_NODE_p));
	psNode2=(sLINK_LIST_NODE_p *)malloc(sizeof(sLINK_LIST_NODE_p));
	psFirstPtr=psNode1;
	psFirstPtrBackUp=psFirstPtr;
	LogPrintf("pFirstPtrBackUp=0x%x\n",psFirstPtrBackUp);
	psNode1->cData="2_Hello";
	psNode1->psNext=NULL;
	psNode2->cData="3_World";
	psNode2->psNext=NULL;
	psNode1->psNext=psNode2;
	LogPrintf("psNode1->cData=%s,psNode2->cData=%s,psNode1->psNext->cData=%s\n",psNode1->cData,psNode2->cData,psNode1->psNext->cData);
	LogPrintf("&sNode1=0x%x, &sNode2=0x%x,sNode1=0x%x, sNode2=0x%x\n",&psNode1,&psNode2,psNode1,psNode2);
	LogPrintf("&(sNode1->cData)=0x%x, &(sNode2->cData)=0x%x,(pFirstPtr+0)=0x%x,(pFirstPtr+1)=0x%x\n",&(psNode1->cData),&(psNode2->cData),(psFirstPtr+0),(psFirstPtr+1));
	//Print List
	if (NULL==psFirstPtr)
	{
	        LogPrintf("The list is empty.\n");
	        return;
    	}
	PushFrontLinkList(&psFirstPtr);
	psFirstPtrBackUp=psFirstPtr;
	psFirstPtr->cData="1_PushFront!";
	while(NULL!=psFirstPtr)	// Traversal
	{
		LogPrintf("pFirstPtr=0x%x,%s\n",psFirstPtr,*psFirstPtr);
		psFirstPtr=psFirstPtr->psNext;
	}
	psFirstPtr=psFirstPtrBackUp;
	PushRearLinkList(&psFirstPtr);
	psFirstPtr->cData="4_PushRear!";
	psFirstPtr=psFirstPtrBackUp;
	while(NULL!=psFirstPtr)	// Traversal
	{
		LogPrintf("pFirstPtr=0x%x,%s\n",psFirstPtr,*psFirstPtr);
		psFirstPtr=psFirstPtr->psNext;
	}
	free(psNode1);
	free(psNode2);
}

void SelectSort(UINT32 *plArray,UINT32 lLength)
{
	UINT32 i,j,lSelect;
	for(i=0;i<lLength;i++)
	{
		lSelect=i;
		for(j=i+1;j<lLength;j++)
			if(plArray[lSelect]>plArray[j])
				lSelect=j;
		SWAP(plArray[i],plArray[lSelect]);
	}
}

UINT32 Partition(UINT32 *plArray,UINT32 lLeft,UINT32 lRight)
{
	UINT32 i=lLeft-1;
	UINT32 j;
	for(j=lLeft;j<lRight;j++)
	{
		if(plArray[j]<=plArray[lRight])
		{
			i++;
			SWAP(plArray[i],plArray[j]);
		}
	}
	SWAP(plArray[i+1],plArray[lRight]);
	return i+1;
}
void QuickSort(UINT32 *plArray,UINT32 lLeft,UINT32 lRight)
{
	UINT32 lQ=0;
	if(lLeft < lRight)
	{
		lQ=Partition(plArray,lLeft,lRight);
		QuickSort(plArray,lLeft,lQ-1);
		QuickSort(plArray,lQ+1,lRight);
	}
}

UINT32 BinarySearch(UINT32 *plArray,UINT32 lLength,UINT32 lTarget)
{
	UINT32 lLeft=0,lRigth=lLength-1,mid=(lLeft+lRigth)/2;
	while(lRigth>=lLeft)
	{
		if(plArray[mid]!=lTarget)
		{
			(plArray[mid]>lTarget)?(lRigth=mid-1):(lLeft=mid+1);
			mid=(lLeft+lRigth)/2;
		}
		else
			return mid;//Success
	}
	return -1;//No target
}

void Accumulate(UINT32 *plSum,UINT32 n)
{//試試用2個for loop
	UINT32 lTemp=n;
	if(n<1)
		return;
	while(lTemp)
	{
		*plSum+=n;
		--lTemp;
	}
	Accumulate(plSum,n-1);
}

UINT32 CalculateDotCount(UINT32 *plImageData,UINT32 lImageSize)
{
	UINT32 lIndex,lIndex2;
	UINT32 lDotCount=0;
	UINT8 *plImageDataBackup;
	plImageDataBackup=(UINT8*)plImageData;
	for (lIndex=0;lIndex<lImageSize;lIndex++)
		for (lIndex2=0;lIndex2<8;lIndex2++)
		{
			if(0!=(plImageDataBackup[lIndex]&(1<<lIndex2)))
			{
				lDotCount++;
				LogPrintf("image_address_backup[%d]&(1<<%d)=%d\n",lIndex,lIndex2,plImageDataBackup[lIndex]&(1<<lIndex2));
			}
		}
	return lDotCount;
}

void FuncTest(UINT8 *pcArray,UINT8 *pcTarget,UINT8 cSize)
{
	ROY_VARD(pcArray[2]);
	ROY_VARD(pcTarget[0]);
	ROY_VARD(cSize);
}
