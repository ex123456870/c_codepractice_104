#include "define_type.h"

#define SWAP(a,b)			{UINT32 lTemp=a;a=b;b=lTemp;}
#define SWAP_BY_XOR(a,b)	{a=a^b;b=a^b;a=a^b;}	//XOR, Same value will fail. XOR technique is considerably slower than using a temporary variable 
#define LENGTH(array)		sizeof(array)/sizeof(array[0])

typedef struct HashNode_t
{
	UINT8 *pcKey;
	void *pData;
	struct HashNode_t *psNext;
}sHashNode;

typedef struct HashTable_t
{
	UINT32 lSize;
	sHashNode **psNodes;
	UINT32 (*HashFunction)(const UINT8 *);
}sHASH_TABLE;

typedef struct Node_p
{
	UINT8 *cData;
	struct Node_p *psNext;
}sLINK_LIST_NODE_p;

typedef struct Node
{
	UINT32 lData;
	struct Node *psNext;
}sLINK_LIST_NODE;

#if 0
extern sHASH_TABLE *CreateHashTable(UINT32 size, UINT32 (*HashFunction)(const UINT8*));
extern void DestroyHashTable(sHASH_TABLE *hashtable);
extern UINT32 InsertElement(sHASH_TABLE *hashtable,const UINT8 *key,void *data);
extern UINT32 RemoveElement(sHASH_TABLE *hashtable,const UINT8 *key);
extern void *GetValue(sHASH_TABLE *hashtable,const UINT8 *key);
extern UINT32 ResizeHashTable(sHASH_TABLE *hashtable,UINT32 size);
#else
extern sHASH_TABLE *CreateHashTable(UINT32 size, UINT32 (*HashFunction)(const UINT8*));
#endif
extern void GetMemoryStringTest(void);
extern void GetMemory(UINT8 **pcGetMemory,UINT8 cLength);
extern void IEEE754_Valf00(float fValue);
extern void IEEE754_Valf01(float *fValue);
extern void IEEE754_Valf02(float fValue);
extern void IEEE754_Valf03(float fValue);
extern void IEEE754_Valf04(double fValue);
extern void PrintMatrix(UINT32 *plArray,UINT32 lCol,UINT32 lRow);
extern void MatrixMul(UINT32 *plArray1,UINT32 *plArray2,UINT32 *plArray3);
extern void MatrixAdd(UINT32 *plArray1,UINT32 *plArray2,UINT32 *plArray3);
extern void (*FuncPointerVoid)(void);
extern void (*FuncPointerVoidTable[2][2])(void);
extern void FuncPointTest_Odd(void);
extern void FuncPointTest_Even(void);
extern void LinkListArray(void);
extern void PushFrontLinkList(sLINK_LIST_NODE_p **pFirstPtr);
extern void PushRearLinkList(sLINK_LIST_NODE_p **pFirstPtr);
extern void LinkListPointer(void);
extern void SelectSort(UINT32 *array,UINT32 length);
extern UINT32 Partition(UINT32 *array,UINT32 left,UINT32 right);
extern void QuickSort(UINT32 *array,UINT32 left,UINT32 right);
extern UINT32 BinarySearch(UINT32 *array,UINT32 length,UINT32 target);
extern void Accumulate(UINT32 *sum,UINT32 n);
extern UINT32 CalculateDotCount(UINT32 *image_address,UINT32 image_size);
extern void FuncTest(UINT8 *array,UINT8 *target,UINT8 size);
