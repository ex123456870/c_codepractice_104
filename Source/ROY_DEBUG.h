#define __ROY__
#ifdef __ROY__
/*
#define ROY_LOG(format,...) \
  do{ \
	  printf("%s:%d@ " ##format"\r\n",__func__,__LINE__,##__VA_ARGS__); \
  }while(0)
*/
#define LogPrintf(fmt, ...) printf("%s:%04d : "fmt, __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define ROY_VAR(var) \
	do{ \
		printf("%s:%d@ %s= 0x%X\r\n",__func__,__LINE__,#var,var); \
	}while(0)
#define ROY_VARD(var) \
	do{ \
		printf("%s:%d@ %s= %d\r\n",__func__,__LINE__,#var,var); \
	}while(0)
#define ROY_VARF(var) \
	do{ \
		printf("%s:%d@ %s= 0x%f\r\n",__func__,__LINE__,#var,var); \
	}while(0)
#define ROY_STRING(string) \
	do{ \
		printf("%s:%d@ %s= %s\r\n",__func__,__LINE__,#string,string); \
	}while(0)
/*
#define ROY_ARRAY(array,start,size,step) \
	do{ { \
		UINT8 volatile *pCur= ((array)+(start)); \
		UINT8 volatile *pEnd = ((array)+(size)); \
		while(pCur <= pEnd){ \
			ROY_LOG(#array"[0x%X] = 0x%X (%d)",pCur,*pCur,*pCur); \
			pCur+=step;\
		} \
	} }while(0)
*/
#else
#define ROY_LOG(format,...)
#define ROY_VAL(var)
#define ROY_VARD(var)
#define ROY_VARF(var)
#define ROY_STRING(string)
#define ROY_ARRAY(array,start,size,step)
#endif
