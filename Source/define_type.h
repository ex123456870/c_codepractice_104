#if !defined(_D_TYPE_H_)
#define _D_TYPE_H_
#define  FALSE			 (0)
#define  TRUE			  (!FALSE)
#define false		FALSE
#define true		TRUE
#define OFF		 false
#define ON		  true
#if !defined(NULL)
#define  NULL			  ((void*)(0))
#endif

typedef enum
{
	DISABLE=0,
	ENABLE=1,
//	FALSE=0,
//	TRUE=1,
//	OFF=0,
//	ON=1,
	CLOSE=0,
	OPEN=1,
	BUSY=0,
	READY=1,
	DISCONNECT=0,
	CONNECT=1,
	IDEL=0,
	FB_SCAN=1,
	ADF_SCAN=2,
	NO_PAPER=1,   /* ADF Sensoer & Doc Senssor*/
	HAS_PAPER=0,  /* ADF Sensoer & Doc Senssor*/
	PAPER_IN_HAS_PAPER=1,
	PAPER_OUT_HAS_PAPER=2,
	PAPER_EJECT_HAS_PAPER=4,
	NG=0,
	OK=1,
	MINUS=0,
	PLUS=1,
	NORMAL_MODE=0,
	TEST_MODE=1,
	WRITE=0,
	READ=1,
	COLOR=0,
	GRAY=1,
	UNLOCK=0,
	LOCK=1
}eSTATUS;

typedef int					BOOL;
typedef unsigned long long	UINT64;
typedef unsigned int		UINT32;
typedef unsigned short		UINT16;
typedef unsigned char		UINT8;
typedef long long			SINT64;
typedef int					SINT32;
typedef short				SINT16;
typedef char				SINT8;
typedef unsigned int		DWORD;
typedef unsigned short		WORD;
typedef unsigned char		BYTE;
typedef double				F64;
typedef float				F32;

#define	XBit8_0	0xfe
#define	XBit8_1	0xfd
#define	XBit8_2	0xfb
#define	XBit8_3	0xf7
#define	XBit8_4	0xef
#define	XBit8_5	0xdf
#define	XBit8_6	0xbf
#define	XBit8_7	0x7f
#define	Bit8_0	0x01
#define	Bit8_1	0x02
#define	Bit8_2	0x04
#define	Bit8_3	0x08
#define	Bit8_4	0x10
#define	Bit8_5	0x20
#define	Bit8_6	0x40
#define	Bit8_7	0x80

#define	XBit16_0	0xfffe
#define	XBit16_1	0xfffd
#define	XBit16_2	0xfffb
#define	XBit16_3	0xfff7
#define	XBit16_4	0xffef
#define	XBit16_5	0xffdf
#define	XBit16_6	0xffbf
#define	XBit16_7	0xff7f
#define	XBit16_8	0xfeff
#define	XBit16_9	0xfdff
#define	Bit16_0		0x0001
#define	Bit16_1		0x0002
#define	Bit16_2		0x0004
#define	Bit16_3		0x0008
#define	Bit16_4		0x0010
#define	Bit16_5		0x0020
#define	Bit16_6		0x0040
#define	Bit16_7		0x0080
#define	Bit16_8		0x0100
#define	Bit16_9 	0x0200
#define	Bit16_10	0x0400
#define	Bit16_11	0x0800
#define	Bit16_12	0x1000
#define	Bit16_13	0x2000
#define	Bit16_14	0x4000
#define	Bit16_15	0x8000

#define	Bit32_0		0x00000001
#define	Bit32_1		0x00000002
#define	Bit32_2		0x00000004
#define	Bit32_3		0x00000008
#define	Bit32_4		0x00000010
#define	Bit32_5		0x00000020
#define	Bit32_6		0x00000040
#define	Bit32_7		0x00000080
#define	Bit32_8		0x00000100
#define	Bit32_9 	0x00000200
#define	Bit32_10	0x00000400
#define	Bit32_11	0x00000800
#define	Bit32_12	0x00001000
#define	Bit32_13	0x00002000
#define	Bit32_14	0x00004000
#define	Bit32_15	0x00008000
#define	Bit32_16	0x00010000
#define	Bit32_17	0x00020000
#define	Bit32_18	0x00040000
#define	Bit32_19	0x00080000
#define	Bit32_20	0x00100000
#define	Bit32_21	0x00200000
#define	Bit32_22	0x00400000
#define	Bit32_23	0x00800000
#define	Bit32_24	0x01000000
#define	Bit32_25 	0x02000000
#define	Bit32_26	0x04000000
#define	Bit32_27	0x08000000
#define	Bit32_28	0x10000000
#define	Bit32_29	0x20000000
#define	Bit32_30	0x40000000
#define	Bit32_31	0x80000000

#define	XBit32_0	0xfffffffe
#define	XBit32_1	0xfffffffd
#define	XBit32_2	0xfffffffb
#define	XBit32_3	0xfffffff7
#define	XBit32_4	0xffffffef
#define	XBit32_5	0xffffffdf
#define	XBit32_6	0xffffffbf
#define	XBit32_7	0xffffff7f
#define	XBit32_8	0xfffffeff
#define	XBit32_9	0xfffffdff
#define	XBit32_10	0xfffffbff
#define	XBit32_11	0xfffff7ff
#define	XBit32_12	0xffffefff
#define	XBit32_13	0xffffdfff
#define	XBit32_14	0xffffbfff
#define	XBit32_15	0xffff7fff
#define	XBit32_16	0xfffeffff
#define	XBit32_17	0xfffdffff
#define	XBit32_18	0xfffbffff
#define	XBit32_19	0xfff7ffff
#define	XBit32_20	0xffefffff
#define	XBit32_21	0xffdfffff
#define	XBit32_22	0xffbfffff
#define	XBit32_23	0xff7fffff
#define	XBit32_24	0xfeffffff
#define	XBit32_25	0xfdffffff
#define	XBit32_26	0xfbffffff
#define	XBit32_27	0xf7ffffff
#define	XBit32_28	0xefffffff
#define	XBit32_29	0xdfffffff
#define	XBit32_30	0xbfffffff
#define	XBit32_31	0x7fffffff
#endif

