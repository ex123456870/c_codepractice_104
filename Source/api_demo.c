#include "api_demo.h"
#define SIZE 10

#if UNION_PRACTICE_TEST
union
{
	F32 fNumber;
	struct
	{
		UINT32 lM:23;
		UINT32 lE:8;
		UINT32 lS:1;
	}sBitField;
}uIEEE754;
#endif

void API_test(void)
{
#if DRAW_INVERTED_TRIANGLE_TEST
	UINT8 cLevel=5;
	DrawInvertedTrangle(cLevel);
	
#elif STRING_TEST
	GetMemoryStringTest();
	
#elif GET_MEMORY_TEST
	UINT8 *cStr=NULL,cIndex=0,cLength=15;
	LogPrintf("&cStr=0x%x,cStr=0x%x\n",&cStr,cStr);
	GetMemory(&cStr,cLength);
	strcpy(cStr,"hello world!!");
	LogPrintf("&cStr=0x%x,cStr=0x%x\n",&cStr,cStr);
	for (cIndex;cIndex<cLength,'\0'!=*(cStr+cIndex);cIndex++)
		printf("%c",*(cStr+cIndex));
	printf("\n");
	LogPrintf("cStr=%s\n",cStr);
	free(cStr);
	
#elif IEEE754_TEST
	F32 f32Value=1.0;
	F64 f64Value=1.0;

	LogPrintf("sizeof(float)=%u,sizeof(double)=%u,sizeof(UINT8)=%u,sizeof(UINT32)=%u\n",sizeof(float),sizeof(double),sizeof(UINT8),sizeof(UINT32));
	IEEE754_Valf00(f32Value);
	IEEE754_Valf01(&f32Value);
	IEEE754_Valf02(f32Value);
	IEEE754_Valf03(f32Value);
	IEEE754_Valf04(f64Value);
	
#elif PRINT_MATRIX_TEST
	UINT32 *ptr=NULL;
	UINT8 cIndex=0,cSide=0;
	UINT32 alMatrix[3][5]={
	{1,2,3,4,5},
	{6,7,8,9,10},
	{11,12,13,14,15}};
	ptr=&alMatrix[0][0];
	cSide=sizeof(alMatrix)/sizeof(alMatrix[0][0]);
	printf("cSide=%u\n",cSide);
	for(cIndex;cIndex<cSide;cIndex++)
		printf("%d\t",*(ptr+cIndex));
	printf("\n");
	PrintMatrix(ptr,0,2);
	PrintMatrix(ptr,1,2);
	PrintMatrix(ptr,2,2);
	PrintMatrix(ptr,3,2);
	PrintMatrix(ptr,4,2);
	
#elif MATRIX_MUL_TEST
	UINT32 *pPtr1,*pPtr2,*pAns;
	UINT32 i,j,matrix1[2][2]={
	{1,2},
	{3,4}};
	UINT32 matrix2[2][2]={
	{4,3},
	{2,1}};
	UINT32 ans_matrix[2][2];
	pPtr1=&matrix1[0][0];
	pPtr2=&matrix2[0][0];
	pAns=&ans_matrix[0][0];
	MatrixMul(pPtr1,pPtr2,pAns);
	for(i=0;i<ROW_MATRIX_MUL;i++)
	{
		for(j=0;j<COLUMN_MATRIX_MUL;j++)
			ROY_VARD(ans_matrix[i][j]);
	}
	
#elif MATRIX_ADD_TEST
	UINT32 *pPtr1,*pPtr2,*pAns;
	UINT32 i,j,matrix1[2][2]={
	{1,2},
	{3,4}};
	UINT32 matrix2[2][2]={
	{4,3},
	{2,1}};
	UINT32 ans_matrix[2][2];
	pPtr1=&matrix1[0][0];
	pPtr2=&matrix2[0][0];
	pAns=&ans_matrix[0][0];
	MatrixAdd(pPtr1,pPtr2,pAns);
	for(i=0;i<ROW_MATRIX_MUL;i++)
	{
		for(j=0;j<COLUMN_MATRIX_MUL;j++)
			ROY_VARD(ans_matrix[i][j]);
	}
	
#elif MATRIX_MUL2_TEST
	
#elif FUNCTION_POINT_TEST
	UINT32 i,lNum=10;
	for(i=0;i<lNum;i++)
	{
		FuncPointerVoid=((i%2)==0)?(FuncPointTest_Even):(FuncPointTest_Odd);
		printf("%d\t",i);
		FuncPointerVoid();
	}
	
#elif COMMUNICATION_CMD_INTERPRETER_TEST
	
#elif FUNCTION_POINT_TABLE_TEST
	//UINT32 i,num=10;
	void (*FuncPointerVoidTable[2][2])(void)={
		{FuncPointTest_Odd,FuncPointTest_Odd},
		{FuncPointTest_Even,FuncPointTest_Even}};
	FuncPointerVoidTable[0][0]();
	FuncPointerVoidTable[0][1]();
	FuncPointerVoidTable[1][0]();
	FuncPointerVoidTable[1][1]();
#elif STRUCT_AND_POINTER_TEST

#elif LINKED_LIST_TEST
	sLINK_LIST_NODE *psFirst1=NULL,*psFirst2=NULL;
	UINT32 alData1[3] = {0,1,2},alData2[3]={2,1,0};

	psFirst1 = CreateLinkedList(alData1,LENGTH(alData1));
	psFirst2 = CreateLinkedList(alData2,LENGTH(alData2));
	psFirst1=ConcatenateLinkedList(psFirst1,psFirst2);
	LinkListTraverse(psFirst1);
	InsertNodeAfterNode(psFirst1,psFirst2,999);
	LinkListTraverse(psFirst1);
	DeleteNodeAfterNode(psFirst1,psFirst2);
	LinkListTraverse(psFirst1);
	InsertNodeBeforeNode(psFirst1,psFirst2,999);
	LinkListTraverse(psFirst1);
	DeleteNodeBeforeNode(psFirst1,psFirst2);
	LinkListTraverse(psFirst1);
	//LinkListArray();
	//LinkListPointer();	
#elif SELECT_SORT_TEST
	UINT32 lIndex;
	UINT32 alArray[SIZE]={1,3,5,7,9,0,22,44,66,88};
	SelectSort(alArray,LENGTH(alArray));
	for(lIndex=0;lIndex<SIZE;lIndex++)
		printf("%d ",alArray[lIndex]);
#elif QUICK_SORT_TEST
/*Pseudocode
QUICKSORT(A, p, r)
	if p < r
		then q <- PARTITION(A, p, r)
			QUICKSORT(A, p, q-1)
			QUICKSORT(A, q+1, r)
end QUICKSORT

PARTITION(A, p, r)
	x <- A[r]
	i <- p-1
	for j <- p to r-1
		do if A[j] <= x
			then  i <- i+1
				exchange A[i]<->A[j]
	exchange A[i+1]<->A[r]
	return i+1
end PARTITION
*/
	UINT32 lIndex,alArray[SIZE]={9,7,5,3,1,0,22,44,66,88};
	QuickSort(alArray,0,LENGTH(alArray)-1);
	for(lIndex=0;lIndex<SIZE;lIndex++)
		printf("%d ",alArray[lIndex]);
#elif BINARY_SEARCH_TEST
	UINT32 lAns,lTarget=3;
	UINT32 alArray[SIZE]={9,7,5,3,1,0,22,44,66,88};
	SelectSort(alArray,LENGTH(alArray));//使用BinarySearch前要先Sorting
	lAns=BinarySearch(alArray,LENGTH(alArray),lTarget);
	ROY_VARD(lAns);
	
#elif ACCUMULATE_TEST
	UINT32 lSum=0,n=5;
	Accumulate(&lSum,n);
	ROY_VARD(lSum);

#elif CALCULATE_DOT_COUNT_TEST
	UINT32 lLine=0xaaaa;
	UINT32 lDot_count=0;
	lDot_count=CalculateDotCount(&lLine,sizeof(lLine));
	LogPrintf("line=0x%x\n",lLine);
	LogPrintf("dot_count=%d\n",lDot_count);

#elif UNION_PRACTICE_TEST
	UINT32 *lTmp;
	uIEEE754.fNumber=-1.0;
	lTmp=(UINT32*)&uIEEE754.fNumber;
	LogPrintf("Float value, Hexadecimal, Sign, Exponent, Mantissa\n");
	LogPrintf("\t%g\t0x%x   %d\t%d\t\t%d\n\n",uIEEE754.fNumber,*lTmp,
	uIEEE754.sBitField.lS,
	uIEEE754.sBitField.lE,
	uIEEE754.sBitField.lM);
	LogPrintf("Size of uIEEE754=%d\n",sizeof(uIEEE754.sBitField));
	LogPrintf("&uIEEE754=0x%x\n",(UINT32)&uIEEE754);
	LogPrintf("&uIEEE754.sBitField=0x%x\n",(UINT32)&uIEEE754.sBitField);
	
#else
	UINT8 acArray[5]={0,1,2,3,4};
	UINT8 cTarget=2;
	FuncTest(acArray,&cTarget,sizeof(acArray)/sizeof(acArray[0]));
#endif

}
